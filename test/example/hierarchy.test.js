const t = require("airtest")();

const noop = () => {};

t.test("root-level case", noop);

t.suite("suite 1", (t) => {
  t.test("suite-level case", noop);
  t.test("suite-level case", noop);

  t.suite("nested suite", (t) => {
    t.test("case in nested suite", noop);
  });

  t.suiteSkip("skipped suite", (t) => {
    t.test("case in skipped suite", noop);
  });
});

t.suite("suite 2", (t) => {
  t.test("skipped case", noop);
  t.test("skipped case", noop);

  t.testOnly("focused case", noop);

  t.suite("skipped suite", (t) => {
    t.test("case in skipped suite", noop);
  });

  t.suiteOnly("focused suite", (t) => {
    t.test("case in focused suite", noop);
  });
});
