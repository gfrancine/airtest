# airtest

An extremely lightweight testing library for NodeJS.

## Usage

Install as a dev dependency:
```
npm i -D airtest
```

Test files end with `.test.js`:
```js
// example.test.js

const t = require("airtest")();

const noop = () => {};

t.test("test case", noop);

t.suite("suite 1", (t) => {
  t.test("suite-level case", noop);

  t.suite("nested suite", (t) => {
    t.test("case in nested suite", noop);
  });

  t.suiteSkip("skipped suite", (t) => {
    t.test("case in skipped suite", noop);
  });
});

t.suite("suite 2", (t) => {
  t.testOnly("focused case", noop);

  t.suiteOnly("focused suite", (t) => {
    t.test("case in focused suite", noop);
  });
});
```

Run at the parent directory:
```
airtest .
```
