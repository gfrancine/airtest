import os from "os";

const ENABLED = (() => {
  // partially taken from supports-color (https://github.com/chalk/supports-color/)
  // Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (https://sindresorhus.com)
  // MIT License

  if (
    /^screen|^xterm|^vt100|^rxvt|color|ansi|cygwin|linux/i
      .test(process.env.TERM || "")
  ) {
    return true;
  }

  if (process.platform === "win32") {
    const osRelease = os.release().split(".");
    return Number(osRelease[0]) >= 10 && Number(osRelease[2]) >= 10586;
  }

  if (process.env.CI) {
    return (
      [
        "TRAVIS",
        "CIRCLECI",
        "APPVEYOR",
        "GITLAB_CI",
        "GITHUB_ACTIONS",
        "BUILDKITE",
        "DRONE",
      ]
        .some((sign) => sign in process.env) ||
      process.env.CI_NAME === "codeship"
    );
  }

  return false;
})();

// from Deno standard library (https://deno.land/std@0.101.0/fmt/colors.ts)
// Copyright 2018-2021 the Deno authors
// MIT License

// todo:
// console.log("\x1b[31m", new Error("a") ,"\x1b[39m")

function color(open: number[], close: number): {
  (message: string): string;
  open: string;
  close: string;
} {
  if (ENABLED) {
    const c = function (message: string) {
      return `\x1b[${open.join(";")}m${message}\x1b[${close}m`;
    };
    c.open = `\x1b[${open.join(";")}m`;
    c.close = `\x1b[${close}m`;
    return c;
  } else {
    const c = function (message: string) {
      return message;
    };
    c.close = "";
    c.open = "";
    return c;
  }
}

export const reset = color([0], 0),
  dim = color([2], 22),
  // foreground
  black = color([30], 39),
  red = color([31], 39),
  green = color([32], 39),
  yellow = color([33], 39),
  blue = color([34], 39),
  magenta = color([35], 39),
  cyan = color([36], 39),
  white = color([37], 39),
  gray = color([90], 39);
