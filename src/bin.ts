#!/usr/bin/env node

import fs from "fs";
import { environment } from "./environment";
import { canAccessPath, filterFiles, isTestFile } from "./util";

const HELP_MESSAGE = `
Usage: airtest [...paths]
`;

async function run(args: string[]) {
  if (args.length === 0 || "--help" in args) {
    console.log(HELP_MESSAGE);
    return;
  }

  const paths = args;
  let files: string[] = [];

  for (let i = 0; i < paths.length; i++) {
    const path = paths[i];

    if (!await canAccessPath(path)) {
      console.log(`Path "${path}" cannot be accessed!`);
      process.exit(1);
    }

    const stat = fs.lstatSync(path);
    if (stat.isDirectory()) {
      const dirfiles = await filterFiles(path, isTestFile);
      files = files.concat(dirfiles);
    } /* file */ else {
      files.push(path);
    }
  }

  const results = await environment.runFiles(files);

  if (!results.success) {
    process.exit(1);
  }
}

run(process.argv.slice(2));
