import type {
  Hook,
  SuiteHandlers,
  SuiteOverview,
  SuiteResult,
  TestFunction,
} from "./types";

import { Case } from "./case";

export class Suite {
  protected hierarchy: string[];
  protected skipped = false;

  protected suites: Suite[] = [];

  protected cases: Case[] = [];
  protected runningCases = 0;
  protected skippedCases = 0;
  protected hasOnly = false;

  protected beforeHooks: Hook[] = [];
  protected afterHooks: Hook[] = [];
  protected beforeEachHooks: Hook[] = [];
  protected afterEachHooks: Hook[] = [];

  protected handlers: SuiteHandlers;

  constructor(opts: {
    skipped?: boolean;
    beforeEach?: Hook[];
    afterEach?: Hook[];
    hierarchy: string[];
    handlers: SuiteHandlers;
  }) {
    this.handlers = opts.handlers;
    this.hierarchy = [...opts.hierarchy];

    if (opts.beforeEach) {
      this.beforeEachHooks = [...opts.beforeEach];
    }

    if (opts.afterEach) {
      this.afterEachHooks = [...opts.afterEach];
    }

    if (opts.skipped !== undefined) {
      this.skipped = opts.skipped;
    }
  }

  protected updateFocused() {
    if (!this.hasOnly) {
      this.hasOnly = true;
      for (let i = 0; i < this.cases.length; i++) {
        this.cases[i].skipped = true;
      }

      for (let i = 0; i < this.suites.length; i++) {
        this.suites[i].skipped = true;
      }
    }
  }

  // cases

  test(headline: string, fn: TestFunction) {
    const shouldSkip = this.hasOnly || this.skipped;

    this.cases.push(
      new Case({
        hierarchy: [...this.hierarchy, headline],
        fn,
        skipped: shouldSkip,
      }),
    );

    if (shouldSkip) {
      this.skippedCases++;
    } else {
      this.skippedCases++;
    }
  }

  testOnly(headline: string, fn: TestFunction) {
    this.updateFocused();

    const shouldSkip = false || this.skipped;

    const testCase = new Case({
      hierarchy: [...this.hierarchy, headline],
      fn,
      skipped: shouldSkip,
    });

    this.cases.push(testCase);

    if (shouldSkip) {
      this.skippedCases++;
    } else {
      this.runningCases++;
    }
  }

  testSkip(headline: string, fn: TestFunction) {
    this.cases.push(
      new Case({
        hierarchy: [...this.hierarchy, headline],
        fn,
        skipped: true,
      }),
    );

    this.skippedCases++;
  }

  // hooks

  before(fn: TestFunction) {
    this.beforeHooks.push({
      kind: "before",
      fn,
    });
  }

  after(fn: TestFunction) {
    this.beforeHooks.push({
      kind: "after",
      fn,
    });
  }

  beforeEach(fn: TestFunction) {
    this.beforeHooks.push({
      kind: "beforeEach",
      fn,
    });
  }

  afterEach(fn: TestFunction) {
    this.beforeHooks.push({
      kind: "afterEach",
      fn,
    });
  }

  // suites

  // DRY
  private makeSuite(headline: string, skipped: boolean) {
    return new Suite({
      skipped: skipped,
      beforeEach: [...this.beforeEachHooks],
      afterEach: [...this.afterEachHooks],
      hierarchy: [...this.hierarchy, headline],
      handlers: { ...this.handlers },
    });
  }

  suite(headline: string, init: (t: Suite) => void) {
    const suite = this.makeSuite(headline, this.hasOnly || this.skipped);
    init(suite);
    this.suites.push(suite);
  }

  suiteSkip(headline: string, init: (t: Suite) => void) {
    const suite = this.makeSuite(headline, true);
    init(suite);
    this.suites.push(suite);
  }

  suiteOnly(headline: string, init: (t: Suite) => void) {
    this.updateFocused();
    const suite = this.makeSuite(headline, false || this.skipped);
    init(suite);
    this.suites.push(suite);
  }

  /** @private */
  getOverview(): SuiteOverview {
    return {
      hierarchy: this.hierarchy,
      skipped: this.skipped,
      runningCases: this.runningCases,
      skippedCases: this.skippedCases,
    };
  }

  /** @private */
  async run() {
    const start = Date.now();
    const result: SuiteResult = {
      hierarchy: [...this.hierarchy],
      success: true,
      failed: 0,
      passed: 0,
      skipped: 0,
      time: 0,
      caseResults: [],
    };

    const runHooks = async (hooks: Hook[]) => {
      for (let i = 0; i < hooks.length; i++) {
        const hook = hooks[i];
        try {
          await hook.fn();
        } catch (e) {
          this.handlers.hookError(hook, e);
        }
      }
    };

    await this.handlers.suiteBegin(this.getOverview());
    await (runHooks(this.beforeHooks));

    for (let i = 0; i < this.cases.length; i++) {
      const testCase = this.cases[i];
      await this.handlers.caseBegin(testCase.getOverview());

      if (testCase.skipped) {
        result.skipped++;
      } else {
        await (runHooks(this.beforeEachHooks));
      }

      const caseResult = await testCase.run();

      if (!testCase.skipped) {
        await (runHooks(this.afterEachHooks));
      }

      if (!caseResult.success) {
        result.failed++;
      } else if (!testCase.skipped) {
        result.passed++;
      }

      await this.handlers.caseResult(caseResult);
      result.caseResults.push(caseResult);
    }

    if (result.failed > 0) {
      result.success = false;
    }

    for (let i = 0; i < this.suites.length; i++) {
      const suite = this.suites[i];
      const suiteResult = await suite.run();

      if (suiteResult.success === false) {
        result.success = false;
      }

      result.caseResults = [...result.caseResults, ...suiteResult.caseResults];
    }

    await (runHooks(this.afterHooks));

    result.time = Date.now() - start;
    await this.handlers.suiteResult(result);

    return result;
  }
}
