import fs from "fs";
import path from "path";

export function human(n: number) {
  n = n / 1e3;

  if (n === 0) {
    return n + " s";
  } else if (n < 1e-3) {
    return (n * 1e6) + " μs";
  } else if (n < 1) {
    return (n * 1e3) + " ms";
  } else {
    return n + " s";
  }
}

export function isTestFile(path: string) {
  return path.slice(-8) === ".test.js";
}

export async function canAccessPath(path: string) {
  try {
    await fs.promises.access(path, fs.constants.F_OK | fs.constants.R_OK);
    return true;
  } catch {
    return false;
  }
}

export async function filterFiles(
  startDir: string,
  filter: (path: string) => boolean,
): Promise<string[]> {
  const files: string[] = [];
  const stack: string[] = [];
  const paths: string[] = []; // the current path ("C:", "Users", "Me" ...)
  stack.push(startDir);

  while (stack.length > 0) {
    const dir = stack.pop() as string;
    paths.push(dir);
    const entries = fs.readdirSync(dir);

    for (let i = 0; i < entries.length; i++) {
      const entry = path.join(...paths, entries[i]);
      if (await canAccessPath(entry)) {
        const entryStat = fs.lstatSync(entry);
        if (entryStat.isDirectory()) {
          stack.push(entry);
        } else if (filter(entry)) {
          files.push(path.resolve(entry));
        }
      }
    }

    paths.pop();
  }

  return files;
}
