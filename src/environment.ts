import { Suite } from "./suite";
import { Reporter } from "./reporter";
import { FileResult, RunResult, SuiteHandlers, TestReporter } from "./types";

class Environment {
  protected reporter: TestReporter = new Reporter();

  protected currentSuite: Suite;
  protected runningFile = "";

  constructor() {
    this.currentSuite = this.makeSuite();
  }

  getCurrentSuite() {
    return this.currentSuite;
  }

  makeSuite() {
    return new Suite({
      hierarchy: [],
      handlers: this.getHandlers(),
    });
  }

  getHandlers(): SuiteHandlers {
    return {
      suiteBegin: async (overview) => {
        await this.reporter.suiteBegin(overview);
      },
      caseBegin: async (overview) => {
        await this.reporter.caseBegin(overview);
      },
      caseResult: async (result) => {
        await this.reporter.caseResult(result);
      },
      suiteResult: async (overview) => {
        await this.reporter.suiteResult(overview);
      },
      hookError: (_hook, error) => {
        throw error;
      },
    };
  }

  async runFiles(paths: string[]): Promise<RunResult> {
    const fileResults: FileResult[] = [];
    const results = {
      success: true,
      fileResults,
      time: 0,
    };

    const start = Date.now();

    for (let i = 0; i < paths.length; i++) {
      const file = paths[i];

      try {
        require(file);
      } catch (e) {
        fileResults.push({
          path: file,
          success: false,
          error: e,
        });
        results.success = false;
        await this.reporter.fileError(file, e);
        continue;
      }

      const suite = this.getCurrentSuite();
      this.currentSuite = this.makeSuite();

      await this.reporter.fileBegin(file, suite.getOverview());

      let fileResult: FileResult;

      try {
        const suiteResult = await suite.run();
        fileResult = {
          path: file,
          success: suiteResult.success,
          suiteResult,
        };

        if (!suiteResult.success) {
          results.success = false;
        }
      } catch (e) {
        fileResult = {
          path: file,
          success: false,
          error: e,
        };
        results.success = false;
        await this.reporter.fileError(file, e);
      }

      fileResults.push(fileResult);
      await this.reporter.fileResult(fileResult);
    }

    results.time = Date.now() - start;
    await this.reporter.runResult(results);
    return results;
  }
}

export const environment = new Environment();
