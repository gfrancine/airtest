import {
  CaseOverview,
  CaseResult,
  FileResult,
  RunResult,
  SuiteOverview,
  SuiteResult,
  TestReporter,
} from "./types";

import * as colors from "./colors";
import { human } from "./util";

export class Reporter implements TestReporter {
  fileBegin(path: string, overview: SuiteOverview) {
    console.log(`\n> Running ${overview.runningCases} tests in ${path}\n`);
  }

  fileError(path: string, _err: unknown) {
    console.log(`\n> ${colors.red("ERROR")} Failed to run ${path}`);
  }

  suiteBegin(overview: SuiteOverview) {
    if (overview.hierarchy.length > 0) {
      const indent = "  ".repeat(overview.hierarchy.length);
      const color = overview.skipped ? colors.gray : (m: string) => m;
      console.log(
        color(indent + overview.hierarchy[overview.hierarchy.length - 1]),
      );
    }
  }

  caseBegin(_overview: CaseOverview) {}

  caseResult(result: CaseResult) {
    const headline = result.hierarchy[result.hierarchy.length - 1];
    const indent = "  ".repeat(result.hierarchy.length);
    if (result.status === "SKIP") {
      console.log(colors.gray(`${indent}SKIP ${headline}`));
    } else {
      const color = result.status === "PASS" ? colors.green : colors.red;
      console.log(`${indent}${color(result.status)} ${headline}`);
    }
  }

  suiteResult(_result: SuiteResult) {}

  fileResult(_result: FileResult) {}

  runResult(result: RunResult) {
    const erroredFiles: (FileResult & { error: unknown })[] = [];
    const erroredCases: {
      hierarchy: string[];
      error: unknown;
      path: string;
    }[] = [];

    let [suitePassed, suiteFailed] = [0, 0];
    let [passed, failed, skipped] = [0, 0, 0];

    for (let i = 0; i < result.fileResults.length; i++) {
      const fileResult = result.fileResults[i];

      if (fileResult.success) {
        suitePassed++;
      } else {
        suiteFailed++;
      }

      if (fileResult.error !== undefined) {
        erroredFiles.push(fileResult as FileResult & { error: unknown });
      }

      if (!fileResult.suiteResult) continue;
      const caseResults = fileResult.suiteResult.caseResults;

      for (let j = 0; j < caseResults.length; j++) {
        const caseResult = caseResults[j];

        if (caseResult.error !== undefined) {
          erroredCases.push({
            ...caseResult,
            error: caseResult.error, // workaround
            path: fileResult.path,
          });
        }

        switch (caseResult.status) {
          case "PASS":
            passed++;
            break;
          case "FAIL":
            failed++;
            break;
          case "SKIP":
            skipped++;
            break;
        }
      }
    }

    // Failures:

    if (erroredFiles.length > 0 || erroredCases.length > 0) {
      console.log("\nFailures:");
    }

    // ERROR C:/Users/Me/Desktop/Projects/a.test.js
    // ...stack

    if (erroredFiles.length > 0) {
      for (let i = 0; i < erroredFiles.length; i++) {
        const erroredFile = erroredFiles[i];
        console.log(`\n${colors.red("ERROR")} ${erroredFile.path}`);
        console.log(erroredFile.error);
      }
    }

    // FAIL "it works"
    // ...stack

    if (erroredCases.length > 0) {
      for (let i = 0; i < erroredCases.length; i++) {
        const erroredCase = erroredCases[i];
        const headline =
          erroredCase.hierarchy[erroredCase.hierarchy.length - 1];
        console.log(`\n${colors.red("FAIL")} "${headline}"`);
        console.log(erroredCase.error);
      }
    }

    // files: 0  passed, 0 failed

    console.log([
      "\nfiles: ",
      suiteFailed === 0
        ? colors.green(`${suitePassed} passed`)
        : `${suitePassed} passed`,
      ", ",
      suiteFailed > 0
        ? colors.red(`${suiteFailed} failed`)
        : `${suiteFailed} failed`,
    ].join(""));

    // cases: 0 passed, 0 failed, 0 skipped, 0 total (2ms)

    console.log([
      "cases: ",
      (failed === 0 ? colors.green(`${passed} passed`) : `${passed} passed`),
      ", ",
      (failed > 0 ? colors.red(`${failed} failed`) : `${failed} failed`),
      ", ",
      `${skipped} skipped, `,
      `${failed + passed} total `,
      `(${human(result.time)})`,
      "\n",
    ].join(""));
  }
}
