import { CaseOverview, CaseResult, TestFunction } from "./types";

export class Case {
  hierarchy: string[];
  fn: TestFunction;
  skipped: boolean;

  constructor(opts: {
    hierarchy: string[];
    fn: TestFunction;
    skipped: boolean;
  }) {
    this.hierarchy = [...opts.hierarchy];
    this.skipped = opts.skipped;
    this.fn = opts.fn;
  }

  getOverview(): CaseOverview {
    return {
      hierarchy: this.hierarchy,
      skipped: this.skipped,
    };
  }

  async run() {
    const caseResult: CaseResult = {
      success: true,
      hierarchy: [...this.hierarchy],
      time: 0,
      status: "SKIP",
    };

    if (!this.skipped) {
      const caseStart = Date.now();
      try {
        await this.fn();
        caseResult.status = "PASS";
      } catch (e) {
        caseResult.error = e;
        caseResult.success = false;
        caseResult.status = "FAIL";
      }
      caseResult.time = Date.now() - caseStart;
    }

    return caseResult;
  }
}
