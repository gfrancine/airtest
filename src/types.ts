export type TestFunction = () => void | Promise<void>;

export type Status = "PASS" | "FAIL" | "SKIP";

export type Hook = {
  kind: "before" | "after" | "beforeEach" | "afterEach";
  fn: TestFunction;
};

export type CaseOverview = {
  hierarchy: string[];
  skipped: boolean;
};

export type CaseResult = {
  hierarchy: string[];
  success: boolean;
  status: Status;
  time: number;
  error?: unknown;
};

export type SuiteOverview = {
  hierarchy: string[];
  skipped: boolean;
  runningCases: number;
  skippedCases: number;
};

export type SuiteResult = {
  hierarchy: string[];
  success: boolean;
  failed: number;
  passed: number;
  skipped: number;
  time: number;
  caseResults: CaseResult[];
};

// an environment has additional information about the file
// file it's running, but a suite should not know about it.
// so there has to be a separate abstraction
export type FileResult = {
  success: boolean;
  path: string;
  suiteResult?: SuiteResult;
  error?: unknown;
};

export type RunResult = {
  time: number;
  success: boolean;
  fileResults: FileResult[];
};

export type SuiteHandlers = {
  suiteBegin: (overview: SuiteOverview) => void | Promise<void>;
  caseBegin: (overview: CaseOverview) => void | Promise<void>;
  caseResult: (result: CaseResult) => void | Promise<void>;
  suiteResult: (result: SuiteResult) => void | Promise<void>;
  hookError: (hook: Hook, error: unknown) => void | Promise<void>;
};

export interface TestReporter {
  fileBegin: (path: string, overview: SuiteOverview) => void | Promise<void>;
  fileError: (path: string, error: unknown) => void | Promise<void>;
  suiteBegin: (overview: SuiteOverview) => void | Promise<void>;
  caseBegin: (overview: CaseOverview) => void | Promise<void>;
  caseResult: (result: CaseResult) => void | Promise<void>;
  suiteResult: (result: SuiteResult) => void | Promise<void>;
  fileResult: (result: FileResult) => void | Promise<void>;
  runResult: (result: RunResult) => void | Promise<void>;
}
